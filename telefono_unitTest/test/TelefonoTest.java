
import entidades.Telefono;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class TelefonoTest {
    // TODO add test methods here.
    
    /*
    * *********** IMPORTANT*******************
    * En Netbeans alt+f6 para ejecutar el test.
    */

    @Test
    public void test() {
        //"^\\+(?:[0-9] ?){6,14}[0-9]$"
        
        List<Telefono> telefonos = new ArrayList<Telefono>();
        
        telefonos.add(new Telefono("+123 123456"));        
        telefonos.add(new Telefono("+12 123456789"));
        telefonos.add(new Telefono("+54 03794568829"));        
        telefonos.add(new Telefono("+54 3794568829"));
        telefonos.add(new Telefono("+54379478829"));       
        
        for(Telefono tel : telefonos)
        {
            assertEquals(true, tel.esValido());            
        }
    }
    
    
    
}
