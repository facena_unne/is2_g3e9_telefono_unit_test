/*
 *Ingenieria del software 2 
 *UNNE 2019
 */
package entidades;

public class Telefono {    
    private String nroTelefono;
    
    public Telefono (String p_nroTelefono){
        this.setNroTelefono(p_nroTelefono);
    }
    /**
     * Retorna true si el valor del atributo nroTelefono es 
     * un numero de teléfono en formato internacional  ITU-T.     
     * @return boolean
     */
    public boolean esValido(){         
        String _regex = "^\\+(?:[0-9] ?){6,14}[0-9]$";
        return this.getNroTelefono().matches(_regex);        
    }     
    public boolean esUnCelular(){
        //implemente la consistencia.
        return false;
    }
    public boolean esUnNumeroDeCorrientes(){
        String _regex = "^\\+54 0?379[4|5][0-9]+$";
        if(this.esValido()){
            return this.getNroTelefono().matches(_regex);
        }
        return false;
    }

    public String getNroTelefono() {
        return nroTelefono;
    }

    public void setNroTelefono(String nroTelefono) {
        this.nroTelefono = nroTelefono;
    }
}


